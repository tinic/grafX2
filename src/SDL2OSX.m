#import <Cocoa/Cocoa.h>

const char *get_paste_board(void)
{
  NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
  NSString *string = [pasteboard stringForType:NSStringPboardType];
  return [string UTF8String];
}
